var CANVAS           = null,
	DRAW             = null,
	BACKGROUND_COLOR = "#000000";

var Clear = function ( ) {
	DRAW.fillStyle = BACKGROUND_COLOR;
	DRAW.fillRect ( 0, 0, CANVAS.width, CANVAS.height );
};

var CreateContext = function ( width, height ) {
	CANVAS = document.createElement ( "CANVAS" );
	CANVAS.width  = ( width  ) ? width  : 800;
	CANVAS.height = ( height ) ? height : 600;
	document.body.appendChild ( CANVAS );
	
	DRAW = CANVAS.getContext ( "2d" );
	Clear ();
};