var Color = function ( r, g, b ) {
	this.r = r;
	this.g = g;
	this.b = b;
};

var HEXColor = function ( color ) {
	var r = color.r.toString ( 16 ),
		g = color.g.toString ( 16 ),
		b = color.b.toString ( 16 );
	
	return "#" +
		( ( color.r > 15 ) ? r : "0" + r ) +
		( ( color.g > 15 ) ? g : "0" + g ) +
		( ( color.b > 15 ) ? b : "0" + b );
};