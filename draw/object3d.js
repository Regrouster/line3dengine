var ScreenDrawObject3D = function ( obj_3D ) {
	if ( !( obj_3D instanceof Object3D ) ) {
		return;
	}
	
	if ( !obj_3D.dp || 
	     !obj_3D.dpolys  ||
		 !obj_3D.dcolors ||
		 !obj_3D.dp.length ||
		 !obj_3D.dpolys.length  ||
		 !obj_3D.dcolors.length    ) 
	{
		return;
	}
	
	var i       = obj_3D.dpolys.length,
	
		points  = obj_3D.dp,
		polys   = obj_3D.dpolys,
		colors  = obj_3D.dcolors,
		
		p0      = null,
		p1      = null,
		p2      = null;
	
	while ( i ) {
		--i;
		
		p0 = points[ polys[ i ][ 0 ] ]; // Vector3
		p1 = points[ polys[ i ][ 1 ] ];
		p2 = points[ polys[ i ][ 2 ] ];
		
		if ( obj_3D.dnormals[ i ].z <= 0 ) {
			ScreenDrawTriangle ( 
				p0.x, p0.y, 
				p1.x, p1.y, 
				p2.x, p2.y, 
				colors[ i ] 
			);
		}
	}
};