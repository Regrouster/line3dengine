/** https://habr.com/ru/post/248159/ **/

var ScreenDrawTriangle = function ( x0, y0, x1, y1, x2, y2, color ) {
	DRAW.beginPath ();
	DRAW.moveTo ( x0, y0 );
	DRAW.lineTo ( x1, y1 );
	DRAW.lineTo ( x2, y2 );
	DRAW.closePath ();
	DRAW.fillStyle = HEXColor ( color );
	DRAW.strokeStyle = HEXColor ( color );
	DRAW.fill ();
	DRAW.stroke ();
};