var FPS = 120,
	DELAY = 1000 / FPS,
	TIMER = null;

var _oldfps = FPS;
var _checkFPS = function () {
	if ( FPS != _oldfps ) {
		_oldfps = FPS;
		DELAY = 1000 / FPS;
	}
}

var LoadScripts = function () {
	Include ( "canvas\\canvas.js" );
	Include ( "geometry\\vector3.js" );
	Include ( "geometry\\object3d.js" );
	Include ( "camera\\camera.js" );
	
	Include ( "math\\sincos.js" );
	
	Include ( "transformation\\reset.js" );
	Include ( "transformation\\move.js" );
	Include ( "transformation\\scale.js" );
	Include ( "transformation\\rotate.js" );
	Include ( "transformation\\project.js" );
	Include ( "transformation\\polysort.js" );
	Include ( "transformation\\normal.js" );
	
	Include ( "draw\\color.js" );
	Include ( "draw\\triangle.js" );
	Include ( "draw\\object3d.js" );
	
	// User scenes
	Include ( "scenes\\test_scene.js" );
};

var _init = function () {
	LoadScripts ();
};

var _start = function () {
	document.body.style.margin = "0px";
	document.body.style.overflow = "hidden";
	CreateContext ( 640, 480 );
	
	GenerateCosSinTables ();
	
	if ( typeof Start === "undefined" ) {
		console.error ( "Where is \"Start\" function?" );
	} else {
		Start ();
	}
	
	if ( typeof Update === "undefined" ) {
		console.error ( "Where is \"Update\" function?" );
	} else {
		_update ();
	}
};

var _update = function () {
	_checkFPS ();
	
	Update ();
	
	TIMER = setTimeout ( _update, DELAY );
};

window.onload = _start;
_init ();