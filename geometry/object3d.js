/*
	points = Vector3 or Arrays(3) or Numbers
	polys  = Array(N) or Numbers=>Array(3)
*/
var Object3D = function ( points, polys, colors, normals ) {
	if ( typeof points === "undefined" ) {
		console.error ( "Empty new Object3D" );
		return;
	}
	
	var i = points.length;
	
	if ( points[ 0 ] instanceof Vector3 ) {
		
		this.points = new Array ( i );
		this.dp     = new Array ( i );
		
		while ( i ) {
			--i;
			this.points[ i ] = Object.assign ( {}, points[ i ] );
			this.dp[ i ] = Object.assign ( {}, points[ i ] );
		}
	} else if ( points[ 0 ] instanceof Array ) {
		
		this.points = new Array ( i );
		this.dp     = new Array ( i );
		
		while ( i ) {
			--i;
			this.points[ i ] = new Vector3 (
				points[ i ][ 0 ],
				points[ i ][ 1 ],
				points[ i ][ 2 ]
			);
			
			this.dp[ i ] = new Vector3 (
				points[ i ][ 0 ],
				points[ i ][ 1 ],
				points[ i ][ 2 ]
			);
		}
	} else {
		
		i = ( i / 3 ) >> 0
		
		this.points = new Array ( i );
		this.dp     = new Array ( i );
		
		while ( i ) {
			--i;
			this.points[ i ] = new Vector3 (
				points[ i * 3 + 0 ],
				points[ i * 3 + 1 ],
				points[ i * 3 + 2 ]
			);
			this.dp[ i ] = new Vector3 (
				points[ i * 3 + 0 ],
				points[ i * 3 + 1 ],
				points[ i * 3 + 2 ]
			);
		}
	}
	
	i = polys.length;
	
	if ( polys[ 0 ] instanceof Array ) {
		
		this.polys  = new Array ( i );
		this.dpolys = new Array ( i );
		
		while ( i ) {
			--i;
			this.polys[ i ]  = polys[ i ].slice ();
			this.dpolys[ i ] = polys[ i ].slice ();
		}
	} else {
		
		i = ( i / 3 ) >> 0;
		
		this.polys  = new Array ( i );
		this.dpolys = new Array ( i );
		
		while ( i ) {
			--i;
			this.polys[ i ] = new Array ( 
				polys[ i * 3 + 0 ],
				polys[ i * 3 + 1 ],
				polys[ i * 3 + 2 ]
			);
			this.dpolys[ i ] = new Array ( 
				polys[ i * 3 + 0 ],
				polys[ i * 3 + 1 ],
				polys[ i * 3 + 2 ]
			);
		}
	}
	
	var i = normals.length;
	
	if ( normals[ 0 ] instanceof Vector3 ) {
		
		this.normals  = new Array ( i );
		this.dnormals = new Array ( i );
		
		while ( i ) {
			--i;
			this.normals[ i ]  = Object.assign ( {}, normals[ i ] );
			this.dnormals[ i ] = Object.assign ( {}, normals[ i ] );
		}
	} else if ( normals[ 0 ] instanceof Array ) {
		
		this.normals  = new Array ( i );
		this.dnormals = new Array ( i );
		
		while ( i ) {
			--i;
			this.normals[ i ] = new Vector3 (
				normals[ i ][ 0 ],
				normals[ i ][ 1 ],
				normals[ i ][ 2 ]
			);
			
			this.dnormals[ i ] = new Vector3 (
				normals[ i ][ 0 ],
				normals[ i ][ 1 ],
				normals[ i ][ 2 ]
			);
		}
	} else {
		
		i = ( i / 3 ) >> 0
		
		this.normals  = new Array ( i );
		this.dnormals = new Array ( i );
		
		while ( i ) {
			--i;
			this.normals[ i ] = new Vector3 (
				normals[ i * 3 + 0 ],
				normals[ i * 3 + 1 ],
				normals[ i * 3 + 2 ]
			);
			this.dnormals[ i ] = new Vector3 (
				normals[ i * 3 + 0 ],
				normals[ i * 3 + 1 ],
				normals[ i * 3 + 2 ]
			);
		}
	}
	
	this.colors  = new Array ( this.polys.length );
	this.dcolors = new Array ( this.polys.length );
	
	i = this.colors.length;
	
	
	if ( !colors ) {
		while ( i ) {
			--i;
			
			this.colors[ i ]  = new Color ( 255, 255, 255 );
			this.dcolors[ i ] = new Color ( 255, 255, 255 );
		}
	} else if ( colors[ 0 ] instanceof Color ) {
		while ( i ) {
			--i;
			
			this.colors[ i ]  = Object.assign ( {}, colors[ i ] );
			this.dcolors[ i ] = Object.assign ( {}, colors[ i ] );
		}
	} else {
		while ( i ) {
			i--;
			
			this.colors[ i ] = new Color ( 
				colors[ i * 3 + 0 ],
				colors[ i * 3 + 1 ],
				colors[ i * 3 + 2 ]
			);
			
			this.dcolors[ i ] = new Color ( 
				colors[ i * 3 + 0 ],
				colors[ i * 3 + 1 ],
				colors[ i * 3 + 2 ]
			);
		}
	}
};