var V3 = {
	back             : function () { return new Vector3 (  0,  0, -1 ); },
	down             : function () { return new Vector3 (  0, -1,  0 ); },
	forward          : function () { return new Vector3 (  0,  0,  1 ); },
	left             : function () { return new Vector3 ( -1,  0,  0 ); },
	negativeInfinity : function () { return new Vector3 ( Number.NEGATIVE_INFINITY, Number.NEGATIVE_INFINITY, Number.NEGATIVE_INFINITY ); },
	one              : function () { return new Vector3 (  1,  1,  1 ); },
	positiveInfinity : function () { return new Vector3 ( Number.POSITIVE_INFINITY, Number.POSITIVE_INFINITY, Number.POSITIVE_INFINITY ); },
	right            : function () { return new Vector3 (  1,  0,  0 ); },
	up               : function () { return new Vector3 (  0,  1,  0 ); },
	zero             : function () { return new Vector3 (  0,  0,  0 ); },
	
	length : function ( a ) {
		return Math.sqrt ( 
			a.x * a.x +
			a.y * a.y +
			a.z * a.z
		);
	},
	
	dot : function ( a ) {
		return a.x * a.x +
		       a.y * a.y +
			   a.z * a.z;
	},
	
	testdot : function ( a ) {
		return this.dot ( a );
	},
	
	add : function ( a, b ) {
		return new Vector3 (
			a.x + b.x,
			a.y + b.y,
			a.z + b.z
		);
	},
	
	addn : function ( a, b ) {
		return new Vector3 (
			a.x + b,
			a.y + b,
			a.z + b
		);
	},
	
	sub : function ( a, b ) {
		return new Vector3 (
			a.x - b.x,
			a.y - b.y,
			a.z - b.z
		);
	},
	
	subn : function ( a, b ) {
		return new Vector3 (
			a.x - b,
			a.y - b,
			a.z - b
		);
	},
	
	mult : function ( a, b ) {
		return new Vector3 (
			a.x *= b.x,
			a.y *= b.y,
			a.z *= b.z
		);
	},
	
	multn : function ( a, b ) {
		return new Vector3 (
			a.x *= b,
			a.y *= b,
			a.z *= b
		);
	},
	
	div : function ( a, b ) {
		return new Vector3 (
			a.x /= b.x,
			a.y /= b.y,
			a.z /= b.z
		);
	},
	
	divn : function ( a, b ) {
		b = ( b ) ? 1 / b : 100000;
		
		return new Vector3 (
			a.x *= b,
			a.y *= b,
			a.z *= b
		);
	},
	
	cross : function ( a, b ) {
		return new Vector3 (
			a.y * b.z - a.z * b.y,
			a.z * b.x - a.x * b.z,
			a.x * b.y - a.y * b.x
		);
	},
	
	normalized : function ( a ) {
		var l = 1 / this.length ( a );
		return new Vector3 (
			a.x * l,
			a.y * l,
			a.z * l
		);
	},
	
	getNormal : function ( a, b, c ) {
        var side1 = this.sub ( b, a ),
            side2 = this.sub ( c, a );

        return this.normalized ( this.cross ( side1, side2 ) );
    },
	
	getCoordNormal : function ( ax, ay, az, bx, by, bz, cx, cy, cz ) {
		return this.getNormal ( 
			new Vector3 ( ax, ay, az ), 
			new Vector3 ( bx, by, bz ), 
			new Vector3 ( cx, cy, cz )
		);
	},
	
	getCenter : function ( p0, p1 ) {
		return new Vector3 (
			( p0.x + p1.x ) * 0.5,
			( p0.y + p1.y ) * 0.5,
			( p0.z + p1.z ) * 0.5
		);
	},
	
	getTriangleCenter : function ( a, b, c ) {
		var f0 = this.getCenter ( a, b ),
			f1 = this.sub ( c, f0 );
			
		f1 = this.divn ( f1, 3 );
			
		return this.add ( f0, f1 );
	}
};

var Vector3 = function ( x, y, z ) {
	this.x = x;
	this.y = y;
	this.z = z;
};