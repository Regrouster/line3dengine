var ALT_PI    = Math.PI / 180,
	D_PI      = 180 / Math.PI,
	COS_TABLE = new Array ( 628 ),
	SIN_TABLE = new Array ( 628 ),
	TR_STEP   = 100;
	
var GenerateCosSinTables = function () {
	var i = COS_TABLE.length;
	while ( i ) {
		--i;
		
		COS_TABLE[ i ] = Math.round ( Math.cos ( i * 0.01 ) * TR_STEP );
		SIN_TABLE[ i ] = Math.round ( Math.sin ( i * 0.01 ) * TR_STEP );
	}
};

var TableCos = function ( angle ) {
	angle *= 100;
	angle >>= 0;
	angle %= 628;
	if ( angle < 0 ) {
		angle = 628 + angle;
	}
	
	return COS_TABLE[ angle ];
};

var TableSin = function ( angle ) {
	angle *= 100;
	angle >>= 0;
	angle %= 628;
	if ( angle < 0 ) {
		angle = 628 + angle;
	}
	
	return SIN_TABLE[ angle ];
};

var IntCos = function ( int_angle ) {
	int_angle %= 628;
	if ( int_angle < 0 ) {
		int_angle = 628 + int_angle;
	}
	
	return COS_TABLE[ int_angle ];
};

var IntSin = function ( int_angle ) {
	int_angle %= 628;
	if ( int_angle < 0 ) {
		int_angle = 628 + int_angle;
	}
	
	return SIN_TABLE[ int_angle ];
};

var DirectCos = function ( direct_angle ) {
	return COS_TABLE[ direct_angle ];
};

var DirectSin = function ( direct_angle ) {
	return SIN_TABLE[ direct_angle ];
};