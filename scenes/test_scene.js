// Test scene
var PI2 = 2 * Math.PI;

var TEST_OBJECT3D = null,
	RX = 0,
	RY = 0,
	RZ = 0;

var Start = function () {
	console.log ( "test_scene Start" );
	
	var pts = [
		-100, -100, -100,
		 100, -100, -100,
		 100,  100, -100,
		-100,  100, -100,
		-100, -100,  100,
		 100, -100,  100,
		 100,  100,  100,
		-100,  100,  100
	];
	
	var pls = [
		0,1,2,  0,2,3, // FRONT
		4,5,6,  4,6,7, // BACK
		0,4,7,  0,7,3, // LEFT
		1,5,6,  1,6,2, // RIGHT
		0,4,5,  0,5,1, // TOP
		3,7,6,  3,6,2  // BOTTOM
	];
	
	var clr = [
		255,0,0,   255,0,0,
		0,255,0,   0,255,0,
		0,0,255,   0,0,255,
		0,255,255, 0,255,255,
		255,255,0, 255,255,0,
		255,0,255, 255,0,255
	];
	
	var nms = [
		 0,0,-1,  0,0,-1,
		 0,0,1,   0,0,1,
		-1,0,0,  -1,0,0,
		 1,0,0,   1,0,0,
		0,-1,0,  0,-1,0,
		0,1,0,   0,1,0
	];
	
	TEST_OBJECT3D = new Object3D ( pts, pls, clr, nms );
};

var Update = function () {
	Clear ();
	
	ResetObject3D   ( TEST_OBJECT3D );
	RotateObject3D  ( TEST_OBJECT3D, RX, RY, RZ, ORDER.xyz );
	NormalObject3D  ( TEST_OBJECT3D );
	OptProjectObject3D ( TEST_OBJECT3D, 600 );
	MoveObject3D    ( TEST_OBJECT3D, 0, 0, 150 );
	SortObject3D    ( TEST_OBJECT3D );
	
	MoveObject3D    ( TEST_OBJECT3D, CANVAS.width * 0.5 >> 0, CANVAS.height * 0.5 >> 0, 0 );
	
	ScreenDrawObject3D ( TEST_OBJECT3D );
	
	RX += 0.005;
	RY += 0.003;
	RZ += 0.001;
	
	RX = ( RX > PI2 ) ? ( RX - PI2 ) : RX;
	RY = ( RY > PI2 ) ? ( RY - PI2 ) : RY;
	RZ = ( RZ > PI2 ) ? ( RZ - PI2 ) : RZ;
};