var NormalObject3D = function ( obj_3D ) {
	var i = obj_3D.dnormals.length;
	
	while ( i-- ) {
		var p0 = obj_3D.dp[ obj_3D.dpolys[ i ][ 0 ] ],
			p1 = obj_3D.dp[ obj_3D.dpolys[ i ][ 1 ] ],
			p2 = obj_3D.dp[ obj_3D.dpolys[ i ][ 2 ] ];
		
		obj_3D.dnormals[ i ] = V3.getTriangleCenter ( p0, p1, p2 );
	}
};