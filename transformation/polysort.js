var SortObject3D = function ( obj_3D ) {
	var t = 0;
	for ( var i = 0, k = obj_3D.dpolys.length; i < k; i++ ) {
		for ( var j = i, l = obj_3D.dpolys.length; j < l; j++ ) {
			if ( obj_3D.dnormals[ j ].z < obj_3D.dnormals[ i ].z ) {
				                      t = obj_3D.dpolys[ j ][ 0 ];
				obj_3D.dpolys[ j ][ 0 ] = obj_3D.dpolys[ i ][ 0 ];
				obj_3D.dpolys[ i ][ 0 ] = t;
				
				                      t = obj_3D.dpolys[ j ][ 1 ];
				obj_3D.dpolys[ j ][ 1 ] = obj_3D.dpolys[ i ][ 1 ];
				obj_3D.dpolys[ i ][ 1 ] = t;
				
				                      t = obj_3D.dpolys[ j ][ 2 ];
				obj_3D.dpolys[ j ][ 2 ] = obj_3D.dpolys[ i ][ 2 ];
				obj_3D.dpolys[ i ][ 2 ] = t;
				
				                      t = obj_3D.dcolors[ j ].r;
				obj_3D.dcolors[ j ].r = obj_3D.dcolors[ i ].r;
				obj_3D.dcolors[ i ].r = t;
				
				                      t = obj_3D.dcolors[ j ].g;
				obj_3D.dcolors[ j ].g = obj_3D.dcolors[ i ].g;
				obj_3D.dcolors[ i ].g = t;
				
				                      t = obj_3D.dcolors[ j ].b;
				obj_3D.dcolors[ j ].b = obj_3D.dcolors[ i ].b;
				obj_3D.dcolors[ i ].b = t;
				
				                      t = obj_3D.dnormals[ j ].x;
				obj_3D.dnormals[ j ].x = obj_3D.dnormals[ i ].x;
				obj_3D.dnormals[ i ].x = t;
				
				                      t = obj_3D.dnormals[ j ].y;
				obj_3D.dnormals[ j ].y = obj_3D.dnormals[ i ].y;
				obj_3D.dnormals[ i ].y = t;
				
				                      t = obj_3D.dnormals[ j ].z;
				obj_3D.dnormals[ j ].z = obj_3D.dnormals[ i ].z;
				obj_3D.dnormals[ i ].z = t;
			}
		}
	}
};