var DIST = 1200;

var ProjectObject3D = function ( obj_3D ) {
	var i    = obj_3D.dp.length,
		dx   = 0,
		dy   = 0,
		dz   = 0,
		dist = 0,
		t    = 0,
		zc   = 0;
	
	while ( i ) {
		--i;
		
		dx  = obj_3D.dp[ i ].x;
		dy  = obj_3D.dp[ i ].y;
		dz  = obj_3D.dp[ i ].z;
		dx *= dx;
		dy *= dy;
		dz *= dz;
		
		dist = Math.sqrt ( dx + dy + dz );
		
		zc   = obj_3D.dp[ i ].z + dist;
		
		t    = dist / ( ( zc ) ? zc : 0.001 );
		
		obj_3D.dp[ i ].x *= t;
		obj_3D.dp[ i ].y *= t;
		obj_3D.dp[ i ].z *= t;
	}
};

var OptProjectObject3D = function ( obj_3D, dist ) {
	var i  = obj_3D.dp.length,
		t  = 0,
		zc = 0;
	
	dist = ( dist ) ? dist : DIST;
	
	while ( i ) {
		--i;
		
		zc = obj_3D.dp[ i ].z + dist;
		
		t = dist / ( ( zc ) ? zc : 0.001 );
		
		obj_3D.dp[ i ].x *= t;
		obj_3D.dp[ i ].y *= t;
		obj_3D.dp[ i ].z *= t;
		
		obj_3D.dnormals[ i ].x *= t;
		obj_3D.dnormals[ i ].y *= t;
		obj_3D.dnormals[ i ].z *= t;
	}
};