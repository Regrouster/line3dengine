var ResetObject3D = function ( obj_3D ) {
	var i = 0,
		j = 0;
	
	i = obj_3D.points.length;
	while ( i-- ) {
		obj_3D.dp[ i ].x = obj_3D.points[ i ].x;
		obj_3D.dp[ i ].y = obj_3D.points[ i ].y;
		obj_3D.dp[ i ].z = obj_3D.points[ i ].z;
	}
	
	i = obj_3D.polys.length;
	while ( i-- ) {
		j = obj_3D.dpolys[ i ].length;
		while ( j-- ) {
			obj_3D.dpolys[ i ][ j ] = obj_3D.polys[ i ][ j ];
		}
	}
	
	i = obj_3D.colors.length;
	while ( i-- ) {
		obj_3D.dcolors[ i ].r = obj_3D.colors[ i ].r;
		obj_3D.dcolors[ i ].g = obj_3D.colors[ i ].g;
		obj_3D.dcolors[ i ].b = obj_3D.colors[ i ].b;
	}
	
	i = obj_3D.normals.length;
	while ( i-- ) {
		obj_3D.dnormals[ i ].x = obj_3D.normals[ i ].x;
		obj_3D.dnormals[ i ].y = obj_3D.normals[ i ].y;
		obj_3D.dnormals[ i ].z = obj_3D.normals[ i ].z;
	}
};