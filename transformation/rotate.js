var ORDER = {
	xyz : 0,
	xzy : 1,
	yxz : 2,
	yzx : 3,
	zxy : 4,
	zyx : 5
};

var RotateXObject3D = function ( obj_3D, rx ) {
	var cx  = TableCos ( rx ),
		sx  = TableSin ( rx ),
		i   = obj_3D.dp.length,
		tmp = 0;
		
	while ( i ) {
		--i;
		
		tmp              = ( obj_3D.dp[ i ].y * cx - obj_3D.dp[ i ].z * sx ) / TR_STEP;
		obj_3D.dp[ i ].z = ( obj_3D.dp[ i ].z * cx + obj_3D.dp[ i ].y * sx ) / TR_STEP;
		obj_3D.dp[ i ].y = tmp;
		
		tmp                    = ( obj_3D.dnormals[ i ].y * cx - obj_3D.dnormals[ i ].z * sx ) / TR_STEP;
		obj_3D.dnormals[ i ].z = ( obj_3D.dnormals[ i ].z * cx + obj_3D.dnormals[ i ].y * sx ) / TR_STEP;
		obj_3D.dnormals[ i ].y = tmp;
	}
};

var RotateYObject3D = function ( obj_3D, ry ) {
	var cy  = TableCos ( ry ),
		sy  = TableSin ( ry ),
		i   = obj_3D.dp.length,
		tmp = 0;
		
	while ( i ) {
		--i;
		
		tmp              = ( obj_3D.dp[ i ].x * cy + obj_3D.dp[ i ].z * sy ) / TR_STEP;
		obj_3D.dp[ i ].z = ( obj_3D.dp[ i ].z * cy - obj_3D.dp[ i ].x * sy ) / TR_STEP;
		obj_3D.dp[ i ].x = tmp;
		
		tmp                    = ( obj_3D.dnormals[ i ].x * cy + obj_3D.dnormals[ i ].z * sy ) / TR_STEP;
		obj_3D.dnormals[ i ].z = ( obj_3D.dnormals[ i ].z * cy - obj_3D.dnormals[ i ].x * sy ) / TR_STEP;
		obj_3D.dnormals[ i ].x = tmp;
	}
};

var RotateZObject3D = function ( obj_3D, rz ) {
	var cz  = TableCos ( rz ),
		sz  = TableSin ( rz ),
		i   = obj_3D.dp.length,
		tmp = 0;
		
	while ( i ) {
		--i;
		
		tmp              = ( obj_3D.dp[ i ].x * cz - obj_3D.dp[ i ].y * sz ) / TR_STEP;
		obj_3D.dp[ i ].y = ( obj_3D.dp[ i ].y * cz + obj_3D.dp[ i ].x * sz ) / TR_STEP;
		obj_3D.dp[ i ].x = tmp;
		
		tmp                    = ( obj_3D.dnormals[ i ].x * cz - obj_3D.dnormals[ i ].y * sz ) / TR_STEP;
		obj_3D.dnormals[ i ].y = ( obj_3D.dnormals[ i ].y * cz + obj_3D.dnormals[ i ].x * sz ) / TR_STEP;
		obj_3D.dnormals[ i ].x = tmp;
	}
};

var RotateObject3D = function ( obj_3D, rx, ry, rz, order ) {
	var f0 = RotateXObject3D,
		f1 = RotateYObject3D,
		f2 = RotateZObject3D;
		
	switch ( order ) {
		case 1  : f0 ( obj_3D, rx ); f2 ( obj_3D, rz ); f1 ( obj_3D, ry ); break;
		case 2  : f1 ( obj_3D, ry ); f0 ( obj_3D, rx ); f2 ( obj_3D, rz ); break;
		case 3  : f1 ( obj_3D, ry ); f2 ( obj_3D, rz ); f0 ( obj_3D, rx ); break;
		case 4  : f2 ( obj_3D, rz ); f0 ( obj_3D, rx ); f1 ( obj_3D, ry ); break;
		case 5  : f2 ( obj_3D, rz ); f1 ( obj_3D, ry ); f0 ( obj_3D, rx ); break;
		default : f0 ( obj_3D, rx ); f1 ( obj_3D, ry ); f2 ( obj_3D, rz ); break;
	}
};