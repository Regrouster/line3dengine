var ScaleObject3D = function ( obj_3D, x, y, z ) {
	var i = obj_3D.dp.length;
	
	while ( i ) {
		--i;
		
		obj_3D.dp[ i ].x *= x;
		obj_3D.dp[ i ].y *= y;
		obj_3D.dp[ i ].z *= z;
	}
};